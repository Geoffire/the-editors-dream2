#ifndef BINARYTREE_H
#define BINARYTREE_H

#include "include.h"


//#define RECURSIVE


template<typename T>
class binaryTree
{
	public:
		binaryTree() {}
		binaryTree(const binaryTree<T> &other);
		binaryTree<T>& operator = (const binaryTree<T> &other);
		~binaryTree();

		binaryTree<T>& operator<<(T *insertMe);     //direct insert
		binaryTree<T>& operator<<(const T &copyMe);  //copy insert

		binaryTree<T>& operator>>(T *&putHere);   //direct pop
		binaryTree<T>& operator>>(T &copyHere);   //copy pop

		void insert(T *insertMe);     //no copy insertion
		void insert(const T &copyMe);  //copy insertion
		bool pop(T *&putHere);		//the least node removed from tree and putHere the points to it (no copy)
		bool pop(T &copyHere);      //the least node removed and copyHere is set equal to it.

		void clear();
		bool empty() const;
		int count() const;

		bool find(const T &findThis) const;   //finds data matching findThis

		int depth() const;
		void rebalance();
		int checkBalance();
		void print();

		template<typename U>
		friend
		std::ostream& operator<<(std::ostream& out, binaryTree<U> &bt);
		template<typename U>
		friend
		std::istream& operator>>(std::istream& in, binaryTree<U> &bt);

	private:
		std::vector<T*> box;  //the only variable

		void nukem();
		void copy(const binaryTree<T> &other);
		int depth(unsigned int e) const;
		int count(unsigned int e) const;
		int totalDataItems(unsigned int e) const;
		void display(unsigned int e, std::ostream& out = std::cout) const;
		unsigned int findNum (const T &s)const;
		unsigned int findNodeNum (const std::string& s)const ;
		unsigned int findLeftMost(unsigned int e);
		unsigned int findRightMost(unsigned int e);
		int insert(unsigned int e, T *insertMe);
		int insert(unsigned int e, const T &copyMe);
		int pop(unsigned int e, T *&putHere);
		void rebalance(unsigned int e, int diff);
		void rotateLeft(unsigned int e);
		void rotateRight(unsigned int e);
		int rebalance(unsigned int e);
		int checkBalance(unsigned int e);

		void move(unsigned int d, unsigned int s);  //preorder
		void moveR(unsigned int d, unsigned int s);  //preorder moves the right before the left, needed for rotate right
		void movePost(unsigned int d, unsigned int s); //postorder
		void movePostR(unsigned int d, unsigned int s); //postorder

		//general helpers
		void swap(unsigned int d, unsigned int s);
		bool is(unsigned int e)const;
		T*& get(unsigned int e);
		const T* get(unsigned int e)const;
		void set(unsigned int e, T *insertMe);
		void add(unsigned int e, const T &insertMe);
		void del(unsigned int e);

};

template<typename T>
binaryTree<T>::binaryTree(const binaryTree<T> &other)   //I didn't feel like writing everything...
{
	copy(other);
}


template<typename T>
binaryTree<T>& binaryTree<T>::operator =(const binaryTree<T> &other)
{
	nukem();
	copy(other);
	return *this;
}

template<typename T>
binaryTree<T>::~binaryTree()
{
	nukem();
}

template<typename T>
inline
binaryTree<T>& binaryTree<T>::operator<<(T *insertMe)
{
	insert(insertMe);
	return *this;
}

template<typename T>
inline
binaryTree<T>& binaryTree<T>::operator<<(const T &copyMe)
{
	insert(copyMe);
	return *this;
}

template<typename T>
inline
binaryTree<T>& binaryTree<T>::operator>>(T *&putHere)
{
	pop(putHere);
	return *this;
}

template<typename T>
inline
binaryTree<T>& binaryTree<T>::operator>>(T &copyHere)
{
	if(pop(copyHere))
		return *this;
	else
		throw EMPTY;
}

template<typename T>
inline
void binaryTree<T>::insert(T *insertMe)  //assumes insertMe is mine
{
#ifdef RECURSIVE
	insert(0,insertMe);  //call recursive version
#else
	//iterative version
	int e = 0;
	while(is(e))  //if there is a node at element e
	{
		if(*insertMe == *get(e))  //does insertMe match the T at e?
		{
			add(e, *insertMe);  //yes then add copyMe to the T at e
			delete insertMe;
			return;  //no rebalance needed as no new T inserted
		}
		else if(*insertMe < *get(e))  //else is it less
			e = left(e);
		else
			e = right(e);
	}
	 //if we didn't return we are at a empty location and have found where the data should be inserted

	set(e, insertMe);   //move insertMe to element e

	int d=1, depthLeft, depthRight;
	while(e>0)
	{
		if(e&1)
		{
			depthLeft = d;
			e = up(e);
			depthRight = depth(right(e));
		}
		else
		{
			depthRight = d;
			e = up(e);
			depthLeft = depth(left(e));
		}

		int diff = depthLeft - depthRight;
		if(!diff)  //if we are completely balanced at this point further up the chain is too on insertion
			return;
		if(std::abs(diff)>1)  //do we need to reballance?
		{
			rebalance(e, diff); //yes
			d = std::max(depthLeft, depthRight);  //this shortcut is acurate on insertion , not removal
		}
		else
			++d;
	}
#endif
}

template<typename T>
inline
void binaryTree<T>::insert(const T& copyMe) //very similar to pointer version, by only makes a copy if inerting.
{
#ifdef RECURSIVE
	insert(0,copyMe);  //call recursive version with a copy of copyMe
#else
	//iterative version
	int e = 0;
	while(is(e))  //if there is a node at element e
	{
		if(copyMe == *get(e))  //does insertMe match the T at e?
		{
			add(e, copyMe);  //yes then add copyMe to the T at e
			return;  //no rebalance needed as no new T inserted
		}
		else if(copyMe < *get(e))  //else is it less
			e = left(e);
		else
			e = right(e);
	}
	 //if we didn't return we are at a empty location and have found where the data should be inserted

	set(e, new T(copyMe));   //move a copy of copyMe to element e

	int d=1, depthLeft, depthRight;
	while(e>0)
	{
		if(e&1)
		{
			depthLeft = d;
			e = up(e);
			depthRight = depth(right(e));
		}
		else
		{
			depthRight = d;
			e = up(e);
			depthLeft = depth(left(e));
		}

		int diff = depthLeft - depthRight;
		if(!diff)  //if we are completely balanced at this point further up the chain is too on insertion
			return;
		if(std::abs(diff)>1)  //do we need to reballance?
		{
			rebalance(e, diff); //yes
			d = std::max(depthLeft, depthRight);  //this shortcut is acurate on insertion , not removal
		}
		else
			++d;
	}
#endif
}

template<typename T>
inline
bool binaryTree<T>::pop(T *&putHere)
{
	if(empty())
		return false;
	pop(0,putHere);

	unsigned int count = box.size();
	for(unsigned int e = count - 1; box[e]; --e)
		--count;
	box.resize(count);

	return true;
}

template<typename T>
inline
bool binaryTree<T>::pop(T &copyHere)
{
	T* hold;
	if(!pop(hold))
		return false;
	copyHere = hold;  //copy
	delete hold;    //and delete the one that was in the tree

	return true;
}

template<typename T>
inline
void binaryTree<T>::clear()
{
	nukem();
}

template<typename T>
inline
bool binaryTree<T>::empty() const
{
	return !is(0);
}


template<typename T>
inline
int binaryTree<T>::count() const
{
	return count(0);  //recursive version
}


template<typename T>
inline
bool binaryTree<T>::find(const T &findThis) const  //the genral find
{
	unsigned int loc = findNum(findThis);
	if(is(loc))
		return 1;
	else
		return 0;
}

template<typename T>
inline
int binaryTree<T>::depth() const
{
	return depth(0);
}

template<typename T>
inline
void binaryTree<T>::rebalance()  //full manual rebalance, isn't needed here but didn't delete it
{
	rebalance(0);
}

template<typename T>
inline
int binaryTree<T>::checkBalance()
{
	return checkBalance(0);
}

template<typename T>
inline
void binaryTree<T>::print()  // print without removing the data (testing)
{
	display(0);
}

///////////////////////////////friends//////////////////////////////////
template<typename U>
std::ostream& operator<<(std::ostream& out, binaryTree<U> &bt)
{
	U* hold;
	while(bt.pop(hold))    //remove
	{
		out<<*hold<<" ";  //print
		delete hold;   //delete
	}
	return out;
}
template<typename U>
std::istream& operator>>(std::istream& in, binaryTree<U> &bt)
{
	U* hold = new U;   //new node
	while(in>>*hold)   //fill it
	{
		bt.insert(hold);  //insert it
		hold = new U;
	}
	delete hold;
	return in;
}

/////////////////////////////////////private functions////////////////////////////////////////////////////

template<typename T>
void binaryTree<T>::nukem()
{
	for(unsigned int i = 0, top = box.size(); i < top; ++i)  //internally we hold Node*ers so we need to delete the nodes manually
	{
		if(T*& hold = box[i])
			delete hold;
	}
	box.clear();
}

template<typename T>
void binaryTree<T>::copy(const binaryTree<T> &other)
{
	unsigned int top = other.box.size();
	box.resize(top,NULL);
	for(unsigned int i; i < top; ++i)
		if(other.box[i])
			box[i] = new T(other.box[i]);
}

template<typename T>
inline
int binaryTree<T>::depth(unsigned int e) const
{
#ifdef RECURSIVE
	return is(e) ? 1 + std::max(depth(left(e)), depth(right(e))) : 0;
#else
	//iterative with no stack - this is over 4x faster than the recursion method
	unsigned int level = 1,
				 factor = e,
				 count = 1,
				 depth = 0,
				 top = box.size();
	for(unsigned int i = 0; factor+i < top && count <= level; ++i,++count)
	{
		if(box[factor + i])
		{
			i += (level - count);
			level<<=1;
			factor = level * e;
			count = 0;
			++depth;
		}
	}
	return depth;

//	for(unsigned int i = 0; count && factor+i < top; --count,++i)  //this algorithm takes almost 2x the time as the one above for some reason, even though I thought it would be faster...
//	{
//		if(box[factor + i])
//		{
//			i <<=1;
//			count<<=1;
//			++count;
//			level<<=1;
//			factor = level * e;
//			++depth;
//		}
//	}
//	return depth;
#endif
}


template<typename T>
int binaryTree<T>::count(unsigned int e) const
{
	return is(e) ? 1 + count(left(e)) + count(right(e)) : 0;
}


template<typename T>
int binaryTree<T>::totalDataItems(unsigned int e) const  //called for nodes only
{
	return is(e) ? get(e)->count() + totalDataItems(left(e)) + totalDataItems(right(e)) : 0;
}


template<typename T>
void binaryTree<T>::display(unsigned int e, std::ostream& out) const  //only in order printing
{
	if(!is(e))
		return;
	display(left(e),out);
	out<<*get(e)<<" ";
	display(right(e),out);
	return;
}

template<typename T>
int binaryTree<T>::insert(unsigned int e, T* insertMe)
{
	int depthLeft, depthRight;
	if(is(e))  //if there is a node at element e
	{
		if(*insertMe == *get(e))  //does insertMe match the T at e?
		{
			add(e, *insertMe);  //yes then add insert me to the T at e
			delete insertMe;    //and delete insertMe
			return -1;  //indicate no rebalance needed as no new T inserted
		}
		else if(*insertMe < *get(e))  //else is it less
		{
			depthLeft = insert(left(e), insertMe); //recursion to the left
			if(depthLeft == -1)  //-1 is the indication no rebalance is needed
				return -1;
			depthRight = depth(right(e));
		}
		else
		{
			depthRight = insert(right(e), insertMe); //else recursion to the right
			if(depthRight == -1)
				return -1;
			depthLeft = depth(left(e));
		}
	}
	else  //if we are at a empty location we found where the data should be inserted
	{
		set(e, insertMe);   //move insertMe to element e
		return 1;
	}
	int diff = depthLeft - depthRight;
	if(std::abs(diff)>1)  //do we need to reballance?
	{
		rebalance(e, diff); //yes
		return std::max(depthLeft, depthRight);  //this shortcut is acurate on insertion , not removal
	}
	if(!diff)  //if we are completely balanced at this point further up the chain is too on insertion
		return -1;
	return (std::max(depthLeft, depthRight) + 1);
}

template<typename T>
int binaryTree<T>::insert(unsigned int e, const T &copyMe)
{
	int depthLeft, depthRight;
	if(is(e))  //if there is a node at element e
	{
		if(copyMe == *get(e))  //does insertMe match the T at e?
		{
			add(e, copyMe);  //yes then add copyMe to the T at e
			return -1;  //indicate no rebalance needed as no new T inserted
		}
		else if(copyMe < *get(e))  //else is it less
		{
			depthLeft = insert(left(e), copyMe); //recursion to the left
			if(depthLeft == -1)  //-1 is the indication no rebalance is needed
				return -1;
			depthRight = depth(right(e));
		}
		else
		{
			depthRight = insert(right(e), copyMe); //else recursion to the right
			if(depthRight == -1)
				return -1;
			depthLeft = depth(left(e));
		}
	}
	else  //if we are at a empty location we found where the data should be inserted
	{
		set(e, new T(copyMe));   //move a copy of copyMe to element e
		return 1;
	}
	int diff = depthLeft - depthRight;
	if(std::abs(diff)>1)  //do we need to reballance?
	{
		rebalance(e, diff); //yes
		return std::max(depthLeft, depthRight);  //this shortcut is acurate on insertion , not removal
	}
	if(!diff)  //if we are completely balanced at this point further up the chain is too on insertion
		return -1;
	return (std::max(depthLeft, depthRight) + 1);
}

template<typename T>
int binaryTree<T>::pop(unsigned int e, T*& putHere)  //assuming putHere doesn't currently point to anything that needs to be deleted
{
	int depthLeft, depthRight;
	if(is(left(e)))  //we are not at the leftmost T yet
	{
		depthLeft = pop(left(e), putHere);  //recursion to the left until we are
		if(depthLeft == -1)
			return -1;
		depthRight = depth(right(e));
	}
	else   //we are at the leftmost T
	{
		putHere = get(e);  //grab the T
		del(e);           //set the element at e to NULL
		if(is(right(e)))   //is there a child?
		{
			swap(e,right(e)); //as we know every sub-tree is balanced there can only be one T to the right at most
			return 1;
		}
		else
			return 0;
	}

	int diff = depthLeft - depthRight;
	if(std::abs(diff)>1)
	{
		rebalance(e, diff);
		depthLeft = depth(left(e));   //here we need to recheck the depth of the left and right to pass the correct value up the chain
		depthRight = depth(right(e));
		diff = depthLeft - depthRight;
	}
	if(diff)  //if there is diff it is +-1, which for removal means our balance isn't changed so no use checking above
		return -1;
	return (std::max(depthLeft, depthRight) + 1);
}


template<typename T>
inline
unsigned int binaryTree<T>::findNum (const T &s)const  //general version iterative find
{
	unsigned int e = 0;
	while(is(e) && !(*get(e) == s))
		s < *get(e) ? e = left(e) : e = right(e);
	return e;
}

template<typename T>
inline
unsigned int binaryTree<T>::findNodeNum (const std::string &s)const  //node version iterative
{
	unsigned int e = 0;
	while(is(e) && !(get(e)->word == s))
		s < get(e)->word ? e = left(e) : e = right(e);
	return e;
}

template<typename T>
inline
unsigned int binaryTree<T>::findLeftMost(unsigned int e)  //assumes element e has data
{
	while (is(left(e)))
		e = left(e);
	return e;
}



template<typename T>
inline
unsigned int binaryTree<T>::findRightMost(unsigned int e) //assumes element e has data
{
	while (is(right(e)))
		e = right(e);
	return e;
}



template<typename T>
inline
void binaryTree<T>::rebalance(unsigned int e, int diff)  //assumes we are called because we need a rebalance
{
	if(diff < -1)
	{
		if((depth(left(right(e))) - depth(right(right(e)))) == 1)  //check the sub balance of the right side
			rotateRight(right(e));   //correct so subtree no sloping the wrong way
		rotateLeft(e);
	}
	else
	{
		if((depth(left(left(e))) - depth(right(left(e)))) == -1)  //opposite of above
			rotateLeft(left(e));
		rotateRight(e);
	}
}


template<typename T>
inline
void binaryTree<T>::rotateLeft(unsigned int e)
{
	movePost(left(left(e)),left(e));  //move the left leg down one to the left
	swap(left(e), e);  //move the root down to the left
	move(right(left(e)),left(right(e)));  //move e->right->left to e->left->right
	move(e,right(e));  //move right up to the root
}


template<typename T>
inline
void binaryTree<T>::rotateRight(unsigned int e)
{
	movePostR(right(right(e)),right(e));  //move the right leg down one to the right
	swap(right(e), e);  //move the root down to the right
	moveR(left(right(e)),right(left(e)));  //move e->left->right to e->right->left
	moveR(e,left(e));  //move left up to the root (right side first)
}

template<typename T>
int binaryTree<T>::rebalance(unsigned int e)  //essentually if a rotate is needed we recheck everything again to make sure balanced below afterwards
{
	if(!is(e))
		return 0;
	while(1)  //repeat until fully balanced
	{
		int depthLeft = rebalance(left(e)),
			depthRight = rebalance(right(e)),
			diff = depthLeft - depthRight;
		if(std::abs(diff)>1)
			rebalance(e, diff);  //call the function that does the rotating
		else
			return (std::max(depthLeft, depthRight) + 1);
	}
}

template<typename T>
int binaryTree<T>::checkBalance(unsigned int e)  //this should be slow but uses no fancy tricks so can check the ones that do.
{
	return !is(e) ? 0 : ((abs(depth(left(e)) - depth(right(e))) > 1 ? 1 : 0) + checkBalance(left(e)) + checkBalance(right(e)));
}

#define RECURSIVE

template<typename T>
inline
void binaryTree<T>::move(unsigned int d, unsigned int s)  //pre order move
{
#ifdef RECURSIVE
	if(!is(s))
		return;
	swap(d,s);
	move(left(d), left(s));
	move(right(d), right(s));
#else
	unsigned int level = 1,
				 dFactor = d,
				 sFactor = s,
				 count = 1;
	for(unsigned int i = 0, top = box.size(); sFactor+i < top; ++i,++count)
	{
		unsigned int sval = sFactor+i;
		if(box[sval])
			swap(dFactor+i,sFactor+i);
		if(count==level)
		{
			level<<=1;
			dFactor = level * d;
			sFactor = level * s;
			count = 0;
		}
	}
#endif
}

template<typename T>
inline
void binaryTree<T>::moveR(unsigned int d, unsigned int s)  //pre order right first version
{
#ifdef RECURSIVE
	if(!is(s))
		return;
	swap(d,s);
	moveR(right(d), right(s));
	moveR(left(d), left(s));
#else
	unsigned int level = 2,
				 dFactor = 2*d,
				 sFactor = 2*s,
				 count = 1;
	if(is(s))
		swap(d,s);
	for(unsigned int i = 1, top = box.size(); sFactor+i < top; ++i,++count)
	{
		int shift = -1;
		if(i&1)
			shift = 1;
		unsigned int sval = sFactor+i+shift;
		if(is(sval))
			swap(dFactor+i+shift,sval);
		if(count==level)
		{
			level<<=1;
			dFactor = level * d;
			sFactor = level * s;
			count = 0;
		}
	}
#endif
}

template<typename T>
void binaryTree<T>::movePost(unsigned int d, unsigned int s)  //post order move
{
	if(!is(s))
		return;
	movePost(left(d), left(s));
	movePost(right(d), right(s));
	swap(d,s);
}
template<typename T>
void binaryTree<T>::movePostR(unsigned int d, unsigned int s)  //post order right first version
{
	if(!is(s))
		return;
	movePostR(right(d), right(s));
	movePostR(left(d), left(s));
	swap(d,s);
}

//general helpers

template<typename T>
inline
void binaryTree<T>::swap(unsigned int d, unsigned int s)  //swap two elements
{
	if (box.size() < d+1)
		box.resize(d+1,NULL);

	box[d] = box[s];  //this is really more of a move...
	box[s] = NULL;    //assuming destination was empty
}


template<typename T>
inline
bool binaryTree<T>::is(unsigned int e) const  //is there a node at element e
{
	if(box.size() < e+1)   //is the vector big enough?
		return false;
	return static_cast<bool>(box[e]); //if so check the element
}


template<typename T>
inline
T*& binaryTree<T>::get(unsigned int e)  //return pointer by reference
{
	return box[e];
}

template<typename T>
inline
const T* binaryTree<T>::get(unsigned int e) const
{
	return box[e];
}

template<typename T>
inline
void binaryTree<T>::set(unsigned int e, T* insertMe)
{
	if(box.size() < e+1)  //resize if needed
		box.resize(e+1,NULL);
	box[e] = insertMe;
}

template<typename T>
inline
void binaryTree<T>::add(unsigned int e, const T &insertMe)
{
	*box[e] += insertMe;   //add insertMe to the T at element e  (what this does depends on the datatype)
}


template<typename T>
inline
void binaryTree<T>::del(unsigned int e)  //delete the element at e
{
	box[e] = NULL;
}

#endif // BINARYTREE_H
