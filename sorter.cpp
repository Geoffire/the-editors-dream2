#include "sorter.h"


Sorter::Sorter()
{
	initialize();
}
Sorter::Sorter(const char* fileName)
{
	initialize();
	P.open(fileName);
}
Sorter::~Sorter()
{
	for(unsigned int i = 0, top = sorted.size(); i < top ; ++i)  //need to empty the sorted vector...
		if(sorted[i])
			delete sorted[i];
}
void Sorter::open(char *fileName)
{
	initialize();
	P.open(fileName);
}
void Sorter::process()
{
	Node* hold;
	Node worker;
	worker.location.resize(2,0);
	iTime(START);
	while(P.get_next(worker.word, worker.location[0], worker.location[1]))  //get all the words;
	{
		box[worker.first()].insert(worker);  //insert it in the right box
		++total;  //up our total
	}
	para = worker.location[0];
	sent = worker.location[1];
	iTime(STOP);

	rTime(START);
	if(box[0].pop(hold)) //we move one node first because in the case of the heap we need to join all the instances of one word before we check if it is a top ten
		sorted.push_back(hold);
	for(unsigned int i = 0; i<26; ++i)  //go through all 26 boxes (one for each letter)
	{
		unsigned int last = 0 ;  //wanted this lasting past the while
		while(box[i].pop(hold))  //pop one
		{
			last = totUnique-1;  //last is the number of the last node in sorted (totals start count at 1, not 0)
#ifdef USING_HEAP  //for the heap only we combine like words
			if(*sorted[last] == *hold)
				*sorted[last] += *hold;
			else  //else it's a new word
#endif
			{
				sorted.push_back(hold);  //add this new word
				++unique[i];  //up our counts
				++totUnique;
				checkTopTen(sorted[last]);  //check if the last word was a top ten
			}
		}
		checkTopTen(sorted[last]);  // need the check the very last (even though its going to be someting starting with Z...
	}
	rTime(STOP);
}

void Sorter::menu()
{
	std::ofstream file;
    int top_ten_choice = 0;
    char menu_choice = '\0';
	std::cout << *this;
	while(menu_choice != 'Q')
    {
		std::cout<< "Q to Quit | S to Save results to file | P to Print all words to file |\nD to Display results | A to display All words | Top 10 Number for details \n\n"
				 << "Make your selection: ";
		std::cin >> top_ten_choice;
		if(std::cin.fail())
		{
			std::cin.clear();
			std::cin >> menu_choice;
			menu_choice = toupper(menu_choice);
			top_ten_choice = 0;
		}

		if(top_ten_choice > 0 && top_ten_choice <= 10)
        {
            std::cout << topTen[top_ten_choice -1]->word << " appears on :" << std::endl
                                                 << *topTen[top_ten_choice - 1] << std::endl;

			std::cout << "Save to file? (y/n) : ";
			std::cin >> menu_choice;

			if(toupper(menu_choice) == 'Y')
				openFile(file) << *topTen[top_ten_choice - 1] << std::endl;

        }
		switch (menu_choice)
		{
		case 'A':
			PrintWords(std::cout);
			break;
		case 'D':
			std::cout<<*this;
			break;
		case 'P':
			PrintWords(openFile(file));
			break;
		case 'S':
			openFile(file)<<*this;
		}
		std::cout<<std::endl;
    }
}

inline
void Sorter::initialize()
{
	for(unsigned int i=0; i < 10; ++i)
		topTen[i] = NULL;
	para=sent=syla=total=0;
	totUnique = 1;
	for(unsigned int i=0; i < 26; ++i)
		unique[i] = 0;
	sorted.clear();
}

inline
void Sorter::checkTopTen (Node* checkThis)
{
	int whichMU = 9;
	for(;whichMU!=0 && !topTen[whichMU]; --whichMU); //find the last mostUsed[] that currenlty points to something or go to the first element if empty
	if(topTen[whichMU])
	{
		for(;whichMU>=0 && topTen[whichMU]->count() < checkThis->count(); --whichMU); //find where we go
		if(whichMU < 9)
		{
			++whichMU;
			for(int j = 9; j>whichMU; --j)  //shift elements after forward one (last gets overwritten)
				topTen[j]=topTen[j-1];
			topTen[whichMU]=checkThis;
		}
	}
	else
		topTen[whichMU] = checkThis;   //our first word
}

void Sorter::PrintWords(std::ostream & out)
{
	char menu_choice;
	for(unsigned int i = 0, top = sorted.size(); i<top ; ++i)
	{
		if(out==std::cout &&  i && !(i%5))
		{

			std::cout << "continue? (Y/N) : ";
			std::cin >> menu_choice;
			if(toupper(menu_choice) == 'N')
				return;
		}
		out<<*sorted[i];
	}
}

std::ostream& operator << (std::ostream& out, const Sorter & S)
{
	double  reading_level = 206.835;
			reading_level-= 1.015*((double)S.total/S.sent);
			reading_level-= 84.6*((double)S.P.syllables/S.total);

	out << "Unique words:"<<S.totUnique<< ", Total Words:"<<S.total<<std::endl
		<< "Paragraphs: "<<S.para<<", Sentances: " << S.sent<< ", Reading level: " << reading_level << std::endl
		<< std::endl
		<< "Words starting with each letter:";
	for(unsigned int i = 0; i < 26; ++i)
	{
		if(!(i%7))
			out<<std::endl;
		out<<(char)('A'+i)<<": "<<std::setw(5)<<S.unique[i]<<" ";
	}
	out << std::endl
		<< std::endl<< "The 10 most common words:"<<std::endl;
	for(unsigned int i = 0; i < 10 && S.topTen[i]; ++i)
		out << "["<<i+1<<"] \""<<S.topTen[i]->word<<"\" used " << S.topTen[i]->count()<<" times."<<std::endl;
	out <<std::endl
		<< "Total Processing Time: "<< S.iTime+S.rTime << ", Insertion Time Only: "<< S.iTime <<std::endl<<std::endl;
	return out;
}


