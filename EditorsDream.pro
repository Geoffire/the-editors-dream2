TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    sorter.cpp

HEADERS += \
    binaryTree.h \
    heap.h \
    include.h \
    node.h \
    parser.h \
    sorter.h
