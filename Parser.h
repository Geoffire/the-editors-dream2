#ifndef PARSER_H
#define PARSER_H

#include "binaryTree.h"

const std::string vowels = "AEIOUY";

inline
bool is_punct(const char &c)
{
    switch(c)
    {
    case '.':
    case '"':
    case ')':
    case '(':
    case '!':
    case '/':
    case ',':
    case '-':
    case '*':
    case '?':
    case ';':
    case '=':
    case ':':
            return true;
            break;
       default:
            return false;
    }
}

inline
bool is_ender(const char &c)
{
    switch(c)
    {
    case '.':
    case '?':
    case '!':
        return true;
        break;
    default:
        return false;
    }
}

inline
bool is_space(const char &c)
{
    switch(c)
    {
    case '\n':
    case ' ':
    case '\r':
    case '\t':
	case '\0':
        return true;
        break;
    default:
        return false;
    }
}

class Parser
{
public:
    Parser();

    void init();
    void open(const char *source_file);
    bool get_next(std::string &word, int &para, int &sent);
    int syllables;

private:
	unsigned int start;
	binaryTree<std::string> contractions;
	binaryTree<std::string> abbreviations;
	std::filebuf fb;
	std::istream scan_file;
	bool qoute, next_sentance;
	std::string line;
	void next_space(std::string &line, unsigned int &pos);
	void string_builder(std::string &word, int &s);

};

inline
Parser::Parser()
    : fb(), scan_file(&fb)
{
    start = -1;
    qoute = false;
    next_sentance = false;
    line = std::string();
    syllables = 0;

    init();
}

inline
void Parser::init()
{
    std::ifstream exp_file("contractions.txt");
    std::string tmp;
    try
    {
        if(exp_file.is_open())
        {
            while(getline(exp_file,tmp))
                contractions<<tmp;
        }
        else if(!exp_file.is_open())
            throw FILE_NOT_OPEN;
        exp_file.close();
    }
    catch(ERRORS)
    {
        std::cout << "Contractions file could not be opened!" << std::endl;
    }

    exp_file.open("abbreviations.txt");

    try
    {
        if(exp_file.is_open())
        {
            while(getline(exp_file,tmp))
                abbreviations<<tmp;
        }
    }
    catch(ERRORS)
    {
        std::cout << "Abbreviations file could not be opened!" << std::endl;
    }
}

inline
void Parser::open(const char* source_file)
{
    try
    {
        fb.open(source_file, std::ios::in);
        if(!fb.is_open())
            throw FILE_NOT_OPEN;
    }
    catch(ERRORS)
    {
        std::cout << "Source file could not be opened!" << std::endl;
    }
}

inline
void Parser::next_space(std::string &line, unsigned int& pos)
{
    while(!is_space(line[pos]))
        pos++;
}

inline
void Parser::string_builder(std::string &word,int &s)
{
    //string_builder will handle sentance incrementing
    if(!line.empty())
    {
        word.clear();

        //Pre processing
        //Removing single qoutes from the beginning of the word.
        while(line[start] == '\'' || is_punct(line[start]))
            start++;

        //If the previous sentance ended on a ." increment the count of the sentance
        if(next_sentance)
        {
            next_sentance = !next_sentance;
            s++;
        }

        //Checking to see if the word is a number. If it is going to the next space.
        if(isdigit(line[start]))
            next_space(line, start);

        while(!is_space(line[start]))
        {

            //Alternating if qoute flag is set
            if(line[start] == '"')
                qoute = !qoute;

            //If it's not in the middle of a qoute increment the sentance count on periods
            if(is_ender(line[start]) && is_space(line[start+1]))
            {
                if(line[start - 1] == '.')
                    start++;

                if(abbreviations.find(word))
                {
                    word+='.';
                    start++;
                }
                else
                    s++;
            }

            //If it's in the middle of a qoute but is ending on a ." set boolean to increment sentance
            if(qoute == true && line[start+1] == '"' && is_ender(line[start]))
                next_sentance = true;

            //If it's not punctuation add it to the word
            if(!is_punct(line[start]))
                word+=(line[start]);

            //If it's a dash remove the dash and go back one
            if(line[start] == '-')
            {
                line[start] = ' ';
                start--;
            }

            if(vowels.npos == vowels.find(toupper(line[start - 1])) && vowels.npos != vowels.find(toupper(line[start])))
                syllables++;

            //Incrementing to the next letter or to the \n or space
            start++;
        }
        //Post processing

        //Making the word uppercase
        word[0] = toupper(word[0]);

        //Removing single qoutes from the end of a word
        //If the word ends in a 's check to see if it is apart of the exceptions (contractions)that are allowed.
        if(word[word.length() - 1] == '\'')
            word.erase(word.length()-1);
        else if(word[word.length() - 2] == '\'' && word[word.length() - 1] == 's')
            if(!contractions.find(word))
                word.erase(word.length() - 2, word.length() - 1);

    }
    //Incrementing past the \n or space
    start++;
}

inline
bool Parser::get_next(std::string &word, int &para, int &sent)
{
    //get_next will be handling paragraph incrementing
    while(start > line.length() || line.empty())
    {
		if (scan_file.eof())
			return false;
        start = 0;
        getline(scan_file, line);

        if(line.empty() && sent > 0)
            para++;
    }

    string_builder(word,sent);
    if (scan_file.eof())
        return false;

    if(word.empty())
       get_next(word,para,sent);

    return true;
}

#endif // PARSER_H
