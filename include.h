#ifndef INCLUDE_H
#define INCLUDE_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>

enum ERRORS { EMPTY, FULL, NOT_FOUND, CANNOT_REMOVE_THAT_MUCH, DONT_DO_THAT, FILE_NOT_OPEN };

inline
std::ofstream& openFile(std::ofstream &ofS)
{
	std::string name;
	while(!ofS.is_open())
	{
		std::cout<<"Where would you like to save? :";
		std::cin>>name;
		ofS.open(name.c_str());
	}
	return ofS;
}

inline
unsigned int left(unsigned int e)
{
	return ((e<<1) + 1);
}

inline
unsigned int right(unsigned int e)
{
	return ((e<<1) + 2);
}

inline
unsigned int up(unsigned int e)
{
	return (((e+1)>>1) - 1);
}

inline
bool operator < (const std::string &l, const std::string &r)
{
	return (l.compare(r) < 0);
}
inline
bool operator == (const std::string &l, const std::string &r)
{
	return (!l.compare(r));
}

enum BUTTONS { START, STOP, RESET, NOTHING };

class timer  //...didn't really need a whole class for this but felt making this somewhat functor like
{
	public:
		timer(BUTTONS b = NOTHING)
		{
			startTime = stopTime = 0;
			this->operator()(b);
		}
		void operator ()(BUTTONS b)
		{
			switch(b)
			{
			case START:
				startTime = clock();
				return;
			case STOP:
				stopTime = clock();
				return;
			case RESET:
				startTime = stopTime = 0;
			default:
				return;
			}
		}
		operator double() const
		{
			if(stopTime)
				return ((stopTime - startTime) / (double) CLOCKS_PER_SEC);
			else
				return ((clock() - startTime) / (double) CLOCKS_PER_SEC);
		}
		double operator + (const timer &other)const
		{
			return (this->operator double() + other.operator double());
		}

		bool running()
		{
			return startTime ? !static_cast<bool>(stopTime) : false;
		}

	private:
		clock_t startTime;
		clock_t stopTime;
};

#endif // INCLUDE_H
